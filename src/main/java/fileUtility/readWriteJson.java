package fileUtility;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

public class readWriteJson {

    public void writeJsonArray(String filePath, String fileName, JSONArray jArray) {
        try (FileWriter file = new FileWriter(filePath + File.separator + fileName)) {
            file.write(jArray.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeJsonObj(String filePath, String fileName, JSONObject jObj) {
        try (FileWriter file = new FileWriter(filePath + File.separator + fileName)) {
            file.write(jObj.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object readJson(String filePath) {
        JSONParser jsonParser = new JSONParser();
        Object obj = null;
        try (FileReader reader = new FileReader(filePath)) {
            obj = jsonParser.parse(reader);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public void getArray(Object object2) throws java.text.ParseException {
        JSONArray jsonArr = (JSONArray) object2;
        for (int k = 0; k < jsonArr.size(); k++) {
            if (jsonArr.get(k) instanceof JSONObject) {
                parseJson(jsonArr.get(k));
            } else {
                System.out.println(jsonArr.get(k));
            }
        }
    }

    public void parseJson(Object object) throws java.text.ParseException {
        JSONObject jsonObject = (JSONObject) object;
        Set set = jsonObject.keySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Object obj = iterator.next();
            if (jsonObject.get(obj) instanceof JSONArray) {
                System.out.println(obj.toString());
                getArray(jsonObject.get(obj));
            } else {
                if (jsonObject.get(obj) instanceof JSONObject) {
                    parseJson(jsonObject.get(obj));
                } else {
                    System.out.println(obj.toString() + "\t" + jsonObject.get(obj));
                }
            }
        }
    }
}
