import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;

import java.io.File;
import java.util.Map;

public class apis {

    public Response callAPIs(Map data, ExtentTest logger) {
        Response response = null;
        if (!data.containsKey("baseURI")) {
            logger.fail("Base URI not provided");
            return response;
        } else if (!data.containsKey("path")) {
            logger.log(Status.INFO, "Base URI: " + (String) data.get("baseURI") + "/");
            logger.fail("Path not provided");
            return response;
        } else if (!data.containsKey("method")) {
            logger.log(Status.INFO, "Base URI: " + (String) data.get("baseURI") + "/" + data.get("path"));
            logger.fail("Method type not provided");
            return response;
        } else {
            logger.log(Status.INFO, "Base URI: " + (String) data.get("baseURI") + "/" + data.get("path"));
        }

        String method = (String) data.get("method");

        RestAssured.baseURI = (String) data.get("baseURI");
        RequestSpecification httpRequest = RestAssured.given();

        if (data.containsKey("header")) {
            httpRequest.headers((Map) data.get("header"));
            logger.log(Status.INFO, "Headers added in the request");
        }
        if (data.containsKey("queryParam")) {
            httpRequest.queryParams((Map) data.get("queryParam"));
            logger.log(Status.INFO, "Query parameters added in the request");
        }
        if (data.containsKey("multipartParams")) {
            Map multipartParams = (Map) data.get("multipartParams");
            for (Object key : multipartParams.keySet()) {
                httpRequest.multiPart((String) key, new File((String) multipartParams.get(key)));
            }
            logger.log(Status.INFO, "Multipart parameter added in the request");
        }
        if (data.containsKey("params")) {
            httpRequest.params((Map<String, ?>) data.get("params"));
            logger.log(Status.INFO, "parameters added in the request");
        }
        if (data.containsKey("body")) {
            httpRequest.body(((JSONObject) data.get("body")).toJSONString());
            logger.log(Status.INFO, "Request body added in the request");
        }

        if (data.containsKey("formParams")) {
            httpRequest.formParams((Map<String, ?>) data.get("formParams"));
        }
        System.out.println(httpRequest.log().uri());

        switch (method.toUpperCase()) {
            case "POST":
                if (data.containsKey("body") || data.containsKey("params") || data.containsKey("multipartParams") || data.containsKey("formParams")) { // Add conditions here
                    response = httpRequest.post((String) data.get("path"));
                } else if (!data.containsKey("multipart")) {
                    logger.fail("Request parameters not provided");
                    return response;
                }
                break;
            case "GET":
                response = httpRequest.get((String) data.get("path"));
                break;
            case "PUT":
                if (data.containsKey("body") || data.containsKey("params") || data.containsKey("multipartParams") || data.containsKey("formParams")) { // Add conditions here
                    response = httpRequest.put((String) data.get("path"));
                } else if (!data.containsKey("multipart")) {
                    logger.fail("Request parameters not provided");
                    return response;
                }
                break;
            default:
                System.out.println("----------------------");
        }

        return response;
    }

}
