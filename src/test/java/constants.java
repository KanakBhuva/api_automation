public class constants {
    //public static String BASE_URI = "https://test.manage.masterdojo.io";
    public static String CONTENT_TYPE = "Application/json";
    public static String CONTENT_TYPE_MULTIPART = "multipart/form-data";
    public static String VERSION = "api/v4.0.0/";
    public static String COMMON_VERSION = "api/v1/";
    public static String PATH_COUNTRIES = COMMON_VERSION + "countries";
    public static String PATH_INDUSTRIES = COMMON_VERSION + "industries";
    public static String LANGUAGE = "en";
    public static String STRIP_KEY = "pk_test_51ED0DTElbB3peMpAJISeE8Kf5d86HlUCw13l2Kx4sCYkOVneDRDJC6jo9GvB7DIMkMFamZgPYG7KWh3r3ddgtVIE00y3lMrOrx";
    public static String PATH_LOGIN = VERSION + "masterdojo/login";
    public static String PATH_CHANGE_PASSWORD = VERSION + "change/psword";
    public static String PATH_LOGOUT = VERSION + "logout";
    public static String PATH_SIGNUP = VERSION + "masterdojo/register";
    public static String PATH_ACCOUNT = VERSION + "account/individual";
    public static String PATH_STRIPE_FILE_URI = "https://files.stripe.com";
    public static String PATH_ACCOUNT_FILE = "v1/files";
    public static String PATH_ACCOUNT_BANK_URI = "https://api.stripe.com";
    public static String PATH_ACCOUNT_BANK = "v1/tokens";
    public static String PATH_KYC_DETAILS =  VERSION + "kyc/detail";
    public static String PATH_USER_LANG =  VERSION + "readLang";  // Response
    public static String PATH_ACCOUNT_UPDATE =  VERSION + "account/external/update";


   // https://common.test.manage.masterdojo.io/api/v1/countries/?lang=en
   // https://common.test.manage.masterdojo.io/api/v1/industries/?lang=en

    // https://test.manage.masterdojo.io/api/v4.0.0/manage/subscriber/profile

    //https://test.manage.masterdojo.io/api/v4.0.0/checkEducatorUrl/knk
    //GET
}
