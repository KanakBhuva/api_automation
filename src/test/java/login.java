import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.google.gson.JsonObject;
import fileUtility.readWriteJson;
import io.restassured.mapper.ObjectMapper;
import io.restassured.mapper.ObjectMapperDeserializationContext;
import io.restassured.mapper.ObjectMapperSerializationContext;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class login extends apis {

    readWriteJson readJson = new readWriteJson();
    JSONObject mainObj = (JSONObject) readJson.readJson(System.getProperty("user.dir") + "\\Data\\API.json");

    public Response signup(ExtentTest logger) {

        Map<String, String> header = new HashMap<>();
        header.put("Content-type", constants.CONTENT_TYPE);

        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("lang", constants.LANGUAGE);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", mainObj.get("URI"));
        data.put("path", constants.PATH_SIGNUP);
        data.put("header", header);
        data.put("queryParam", queryParam);
        data.put("body", mainObj.get("Signup"));
        data.put("method", "POST");

        return callAPIs(data, logger);
    }

    public Response updatePersonalDetails(String token, ExtentTest logger) {
        JSONObject kyc = (JSONObject) mainObj.get("KYC");

        Map<String, String> header = new HashMap<>();
        header.put("Content-type", constants.CONTENT_TYPE);
        header.put("x-auth-token", token);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", mainObj.get("URI"));
        data.put("path", constants.PATH_ACCOUNT);
        data.put("header", header);
        data.put("body", kyc.get("PersonalDetails"));
        data.put("method", "PUT");

        return callAPIs(data, logger);
    }

    public Response updateBankDetails(String token, ExtentTest logger) {
        JSONObject bankDetails = new JSONObject();
        JSONObject kyc = (JSONObject) mainObj.get("KYC");
        JSONObject bdObj = (JSONObject) kyc.get("BankDetails");
        bankDetails.put("accountBSBNumber", (String) bdObj.get("routing_number"));
        bankDetails.put("accountHolderName", (String) bdObj.get("account_holder_name"));
        bankDetails.put("accountHolderType", (String) bdObj.get("account_holder_type"));
        bankDetails.put("accountNumber", (String) bdObj.get("account_number"));
        bankDetails.put("country", (String) bdObj.get("country_alpha_two_code"));
        bankDetails.put("currency", (String) bdObj.get("currency"));
        bankDetails.put("object", "bank_account");
        bankDetails.put("platformSubMerchantCode", "-");

        Map<String, String> header = new HashMap<>();
        header.put("Content-type", constants.CONTENT_TYPE);
        header.put("x-auth-token", token);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", mainObj.get("URI"));
        data.put("path", constants.PATH_ACCOUNT_UPDATE);
        data.put("header", header);
        data.put("body", bankDetails);
        data.put("method", "PUT");

        return callAPIs(data, logger);
    }

    public Response addBankDetailsInStripe(ExtentTest logger) {
        JSONObject kyc = (JSONObject) mainObj.get("KYC");
        Map<String, Object> bankDetails = new HashMap<>(); // kyc.get("BankDetails");
        bankDetails.put("bank_account[country]", ((JSONObject) kyc.get("BankDetails")).get("country_alpha_two_code"));
        bankDetails.put("bank_account[currency]", ((JSONObject) kyc.get("BankDetails")).get("currency"));
        bankDetails.put("bank_account[account_holder_name]", ((JSONObject) kyc.get("BankDetails")).get("account_holder_name"));
        bankDetails.put("bank_account[account_holder_type]", ((JSONObject) kyc.get("BankDetails")).get("account_holder_type"));
        bankDetails.put("bank_account[routing_number]", ((JSONObject) kyc.get("BankDetails")).get("routing_number"));
        bankDetails.put("bank_account[account_number]", ((JSONObject) kyc.get("BankDetails")).get("account_number"));

        Map<String, String> header = new HashMap<>();
        header.put("authorization", "Bearer " + constants.STRIP_KEY);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", constants.PATH_ACCOUNT_BANK_URI);
        data.put("path", constants.PATH_ACCOUNT_BANK);
        data.put("header", header);
        data.put("formParams", bankDetails);
        data.put("method", "POST");

        return callAPIs(data, logger);
    }

    public Response addBankDetails(String token, ExtentTest logger) {
        String id;
        Response res = addBankDetailsInStripe(logger);
        if (res.statusCode() == 200) {
            id = (String) ((JsonPath) res.getBody().jsonPath()).getString("id");
            JSONObject body = new JSONObject();
            body.put("externalAccountToken", id); //

            Map<String, String> header = new HashMap<>();
            header.put("Content-type", constants.CONTENT_TYPE);
            header.put("x-auth-token", token);

            Map<String, Object> data = new HashMap<>();
            data.put("baseURI", mainObj.get("URI"));
            data.put("path", constants.PATH_ACCOUNT);
            data.put("header", header);
            data.put("body", body);
            data.put("method", "PUT");

            return callAPIs(data, logger);
        } else {
            logger.log(Status.FAIL, "Uploading failed");
        }
        return res;
    }

    public Response uploadFileInStripe(String documentType, Boolean isFrontView, ExtentTest logger) {
        JSONObject kyc = (JSONObject) mainObj.get("KYC");
        Map<String, String> header = new HashMap<>();
        header.put("authorization", "Bearer " + constants.STRIP_KEY);

        Map<String, String> params = new HashMap<>();
        params.put("purpose", "identity_document");

        Map<String, String> multipartParams = new HashMap<>();
        if (documentType.toUpperCase().equals("PHOTOID") && isFrontView) {
            multipartParams.put("file", (String) ((JSONObject) ((JSONObject) kyc.get("Documents")).get("PhotoID")).get("Front"));
        } else if (documentType.toUpperCase().equals("PHOTOID") && !isFrontView) {
            multipartParams.put("file", (String) ((JSONObject) ((JSONObject) kyc.get("Documents")).get("PhotoID")).get("Back"));
        } else if (documentType.toUpperCase().equals("ADDRESSID")) {
            multipartParams.put("file", (String) ((JSONObject) kyc.get("Documents")).get("AddressID"));
        }

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", constants.PATH_STRIPE_FILE_URI);
        data.put("path", constants.PATH_ACCOUNT_FILE);
        data.put("header", header);
        data.put("multipartParams", multipartParams);
        data.put("params", params);
        data.put("method", "POST");

        return callAPIs(data, logger);
    }

    public Response updateDocuments(String token, String documentType, Boolean isFrontView, ExtentTest logger) {
        String id;
        Response res = uploadFileInStripe(documentType, isFrontView, logger);
        if (res.statusCode() == 200) {
            id = (String) ((JsonPath) res.getBody().jsonPath()).getString("id");
            JSONObject body = new JSONObject();
            if (documentType.toUpperCase().equals("PHOTOID") && isFrontView) {
                body.put("documentFront", id);
            } else if (documentType.toUpperCase().equals("PHOTOID") && !isFrontView) {
                body.put("documentBack", id);
            } else if (documentType.toUpperCase().equals("ADDRESSID")) {
                body.put("documentAdditional", id);
            }

            Map<String, String> header = new HashMap<>();
            header.put("Content-type", constants.CONTENT_TYPE);
            header.put("x-auth-token", token);

            Map<String, Object> data = new HashMap<>();
            data.put("baseURI", mainObj.get("URI"));
            data.put("path", constants.PATH_ACCOUNT);
            data.put("header", header);
            data.put("body", body);
            data.put("method", "PUT");

            return callAPIs(data, logger);
        } else {
            logger.log(Status.FAIL, "Uploading failed");
        }
        return res;
    }

    public Response getKYCDetails(String token, ExtentTest logger) {
        Map<String, String> header = new HashMap<>();
        header.put("Content-type", constants.CONTENT_TYPE);
        header.put("x-auth-token", token);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", mainObj.get("URI"));
        data.put("path", constants.PATH_KYC_DETAILS);
        data.put("header", header);
        data.put("method", "GET");

        return callAPIs(data, logger);
    }

    public Response login(ExtentTest logger) {

        Map<String, String> header = new HashMap<>();
        header.put("Content-type", constants.CONTENT_TYPE);

        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("lang", constants.LANGUAGE);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", mainObj.get("URI"));
        data.put("path", constants.PATH_LOGIN);
        data.put("header", header);
        data.put("queryParam", queryParam);
        data.put("body", mainObj.get("Login"));
        data.put("method", "POST");

        return callAPIs(data, logger);
    }

    public Response login_withNewPassword(ExtentTest logger) {

        Map<String, String> header = new HashMap<>();
        header.put("Content-type", constants.CONTENT_TYPE);

        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("lang", constants.LANGUAGE);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", mainObj.get("URI"));
        data.put("path", constants.PATH_LOGIN);
        data.put("header", header);
        data.put("queryParam", queryParam);
        data.put("body", mainObj.get("LoginWithNewPassword"));
        data.put("method", "POST");

        return callAPIs(data, logger);
    }

    public Response changePassword(String token, ExtentTest logger) {

        Map<String, String> header = new HashMap<>();
        header.put("Content-type", constants.CONTENT_TYPE);
        header.put("x-auth-token", token);

        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("lang", constants.LANGUAGE);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", mainObj.get("URI"));
        data.put("path", constants.PATH_CHANGE_PASSWORD);
        data.put("header", header);
        data.put("queryParam", queryParam);
        data.put("body", mainObj.get("ChangePassword"));
        data.put("method", "POST");

        return callAPIs(data, logger);
    }

    public Response getUserLanguage(String token, ExtentTest logger) {
        Map<String, String> header = new HashMap<>();
        header.put("Content-type", constants.CONTENT_TYPE);
        header.put("x-auth-token", token);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", mainObj.get("URI"));
        data.put("path", constants.PATH_USER_LANG);
        data.put("header", header);
        data.put("method", "GET");

        return callAPIs(data, logger);
    }

    public Response logout(String token, ExtentTest logger) {

        Map<String, String> header = new HashMap<>();
        header.put("Content-type", constants.CONTENT_TYPE);
        header.put("x-auth-token", token);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", mainObj.get("URI"));
        data.put("path", constants.PATH_LOGOUT);
        data.put("header", header);
        data.put("method", "GET");

        return callAPIs(data, logger);
    }

    public Response getCountries(String token, ExtentTest logger) {

        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("lang", constants.LANGUAGE);

        Map<String, Object> data = new HashMap<>();
        data.put("baseURI", mainObj.get("common_URI"));
        data.put("path", constants.PATH_COUNTRIES);
        data.put("method", "GET");
        data.put("queryParam", queryParam);

        return callAPIs(data, logger);
    }

}
