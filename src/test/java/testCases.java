import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class testCases extends apis {

    public ExtentHtmlReporter htmlReporter;
    public ExtentReports report;
    public ExtentTest mdTest;
    Response res;
    String token, lang;
    login login = new login();

    @BeforeClass(alwaysRun = true)
    public void initializeReport() {
        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "\\Reports\\AutomationReport.html");
        //System.out.println(System.getProperty("user.dir"));
        report = new ExtentReports();
        report.attachReporter(htmlReporter);
        report.setSystemInfo("Published By", "MVP1Ventures");
        report.setSystemInfo("Environment", "Test");
        report.setSystemInfo("Made By", "Kanak Bhuva");
        htmlReporter.config().setDocumentTitle("MD - API Automation");
        htmlReporter.config().setReportName("MD - API Automation Report");
    }

    @Test(priority = 1, enabled = true, groups = {"Demo"})
    public void login1() throws AssertionError {
        mdTest = report.createTest("Test Case: Login");
        res = login.login(mdTest);
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "Successfully login");
        token = res.getHeader("x-auth-token");
        res = login.changePassword(token, mdTest);
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "password has been changed");
        res = login.login_withNewPassword(mdTest);
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "Successfully login using new password");
        res = login.logout(token, mdTest);
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "Successfully logout");
        //mdTest.log(Status.INFO, "Response code is " + res.statusCode());
        //System.out.println("Response body is =>  " + response.getBody().asString());
    }

    @Test(priority = 1, enabled = true, groups = {"Demo1"})
    public void login() throws AssertionError {
        mdTest = report.createTest("Test Case: Login");
        res = login.login(mdTest);
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "Successfully login");
        token = res.getHeader("x-auth-token");
    }

    @Test(priority = 2, enabled = true, groups = {"Demo"})
    public void getUserLang() throws AssertionError {
        mdTest = report.createTest("Test Case: Get user language");
        res = login.getUserLanguage(token, mdTest);
        Assert.assertEquals(res.statusCode(), 200);
        lang = (String) ((JsonPath) res.getBody().jsonPath()).get("message");
        mdTest.log(Status.INFO, "Default language: " + lang);
        //System.out.println("Response body is =>  " + res.getBody().asString());
    }

    @Test(priority = 3, enabled = true, groups = {"Demo"})
    public void kyc() throws AssertionError {
        mdTest = report.createTest("Test Case: KYC");
        String status;
        res = login.updatePersonalDetails(token, mdTest);
        System.out.println("Response body is =>  " + res.getBody().asString());
        System.out.println("Response code is =>  " + res.statusCode());
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "Response code is " + res.statusCode());
        mdTest.log(Status.INFO, "Personal details updated successfully");

        res = login.addBankDetails(token, mdTest);
        System.out.println("Response body is =>  " + res.getBody().asString());
        System.out.println("Response code is =>  " + res.statusCode());
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "Response code is " + res.statusCode());
        mdTest.log(Status.INFO, "Bank details updated successfully");

        res = login.updateDocuments(token, "PhotoID", true, mdTest);
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "Response code is " + res.statusCode());
        mdTest.log(Status.INFO, "Identification document: Front view added in the account successfully");

        res = login.updateDocuments(token, "PhotoID", false, mdTest);
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "Response code is " + res.statusCode());
        mdTest.log(Status.INFO, "Identification document: Front view added in the account successfully");

        res = login.updateDocuments(token, "AddressID", true, mdTest);
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "Response code is " + res.statusCode());
        mdTest.log(Status.INFO, "Identification document: Front view added in the account successfully");

        res = login.getKYCDetails(token, mdTest);
        Assert.assertEquals(res.statusCode(), 200);
        status = (String) ((JsonPath) res.getBody().jsonPath()).get("message.status");
        mdTest.log(Status.INFO, "KYC status: " + status);
        if(status.equals("unverified")) {
            mdTest.fail("KYC: Not verified");
        }else {
            mdTest.log(Status.INFO, "KYC completed successfully");
        }
        //System.out.println("Response body is =>  " + res.getBody().asString());
        //System.out.println("Response code is =>  " + res.statusCode());
    }

    @Test(priority = 4, enabled = true, groups = {"Demo1"})
    public void updateBankDetails_KYC() throws AssertionError {
        mdTest = report.createTest("Test Case: Update bank details");
        res = login.addBankDetails(token, mdTest);
        System.out.println("Response body is =>  " + res.getBody().asString());
        Assert.assertEquals(res.statusCode(), 200);
        mdTest.log(Status.INFO, "Bank details updated successfully");
    }

    @AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result) throws Exception {
        if (result.getStatus() == ITestResult.FAILURE) {
            if (res == null) {
                mdTest.log(Status.FAIL, "NULL response received");
            } else {
                mdTest.log(Status.INFO, "Response body is " + res.asString());
            }
            mdTest.fail(result.getName() + " Test Case FAILED");
        } else if (result.getStatus() == ITestResult.SKIP) {
            mdTest.skip(MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));
        }
    }

    @AfterClass(alwaysRun = true)
    public void endReport() {
        report.flush();
    }

}
